package si.uni_lj.fri.pbd.miniapp2

import android.app.Service
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Binder
import android.os.IBinder
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import kotlin.math.abs


class AccelerationService : Service(), SensorEventListener {

    companion object {
        private val TAG: String? = AccelerationService::class.simpleName
        const val ACTION_STOP = "stop_service"
        const val threshold = 2;


    }


    //var mediaPlayerService: MediaPlayerService? = MediaPlayerService()

    private lateinit var sensorManager: SensorManager
    private lateinit var sensor: Sensor



    override fun onCreate() {
        super.onCreate()
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        }
       sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)

    }

    private var serviceBinder: AccelerationService.RunServiceBinder = RunServiceBinder();
    //for binding to mediaplayer service
    inner class RunServiceBinder : Binder() {
        val service: AccelerationService
            get() = this@AccelerationService
    }

    override fun onBind(intent: Intent): IBinder {
        return serviceBinder
    }


    fun background() {
        stopForeground(true)
    }


    override fun onDestroy() {
        super.onDestroy()
        sensorManager.unregisterListener(this)
        Log.d(TAG, "Destroying service")
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }


    private var command = "";
    private var time : Long = 0;
    private var diff : Long = 0
    private var lastShake : Long = 0;


    private var prevX = 0
    private var prevY = 0
    private var prevZ = 0
    private var x = 0
    private var y = 0
    private var z = 0
    private var dx = 0
    private var dy = 0
    private var dz = 0



    var prevTime : Long = 0;


    //senses vertical or horizontal movement
    override  fun onSensorChanged(event: SensorEvent?) {
        event?:return
        time = event.timestamp

        x = event.values[0].toInt()
        y = event.values[1].toInt()
        z = event.values[2].toInt()



        if(prevTime ==  0.toLong()){
            prevTime = time
            lastShake = time
            prevX = x;
            prevY = y;
            prevZ = z;
        }
        else {
            diff = (time-prevTime)
            if(diff > 0){
                dx = abs(prevX-x)
                dy = abs(prevY-y)
                dz = abs(prevZ-z)

                if(dx<threshold) dx = 0
                if(dy<threshold) dy = 0
                if(dz<threshold) dz = 0

                if(dx!=0 || dy !=0){
                    if((time-lastShake)/1000000 > 500){
                        command = getString(R.string.idle)

                        if(dx>dy) {
                            command = getString(R.string.horizontal)


                        }
                        if(dy>dx){
                            command = getString(R.string.vertical)

                        }

                        if(command != getString(R.string.idle) ){
                            sendMessage()

                        }
                    }
                    lastShake = time;
                }
                prevX = x;
                prevY = y;
                prevZ = z;
                prevTime = time;

            }
        }

    }


//sends broadcast message
    private fun sendMessage() {
        // The string "my-message" will be used to filer the intent
        val intent = Intent("command")
        // Adding some data
        intent.putExtra("command", command)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }
}