package si.uni_lj.fri.pbd.miniapp2

import android.annotation.SuppressLint
import android.app.PendingIntent.getActivity
import android.content.*
import android.os.*
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import kotlinx.coroutines.*
import si.uni_lj.fri.pbd.miniapp2.databinding.ActivityMainBinding
import si.uni_lj.fri.pbd.miniapp2.MediaPlayerService.Companion.ACTION_START
import java.lang.ref.WeakReference

import kotlin.system.exitProcess

class MainActivity : AppCompatActivity() {

    private val processingScope = CoroutineScope(Dispatchers.IO)

    companion object {
        val TAG = MainActivity::class.simpleName

        // Message type for the handler
        private const val MSG_UPDATE_TIME = 1
        private const val UPDATE_RATE_MS = 1000L
    }

    var mediaPlayerService: MediaPlayerService? = MediaPlayerService()



    var serviceBound: Boolean = false


    //connection to mediaplayerservice
    private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            Log.d(TAG, "Service bound")
            val binder = iBinder as MediaPlayerService.RunServiceBinder
            mediaPlayerService = binder.service
            serviceBound = true
            mediaPlayerService!!.background()

            //return UI TO PREVIOUS STATE if song is paused
            if (mediaPlayerService!!.songPaused) {

                disableStopAndPause()
                binding.durationInformation.text = mediaPlayerService!!.helper(0)
                binding.trackTitle.text = mediaPlayerService?.songName

            }
            //return UI TO PREVIOUS STATE if song is stopped
            if (mediaPlayerService!!.songStopped) {
                disableStopAndPause()
                //00:00
                binding.durationInformation.text = mediaPlayerService!!.helper(-1)
                //clickplay
                binding.trackTitle.text = mediaPlayerService!!.songName
            }

            //update the ui if song is playing
            if (mediaPlayerService?.player?.isPlaying!!) {
                updateUIStartRun()
            }

            if(mediaPlayerService!!.gesturesOn){
                //enable goff button
                binding.gofButton.isClickable=true
                binding.gofButton.text= getString(R.string.g_off)

                //disable gon button
                binding.gonButton.isClickable=false
                binding.gonButton.text= null
            }

            if(!mediaPlayerService!!.gesturesOn){
                //enable gon button
                binding.gonButton.isClickable=true
                binding.gonButton.text= getString(R.string.g_on)

                //disable goff button
                binding.gofButton.isClickable=false
                binding.gofButton.text= null
            }


            processingScope.launch { doProcessing() }
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            Log.d(TAG, "Service disconnect")
            serviceBound = false
        }
    }


    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root


        setContentView(view)

    }


    override fun onStart() {
        super.onStart()
        //for recievig broadcast messages
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,  IntentFilter("command"));
        val i = Intent(this, MediaPlayerService::class.java)
        i.action = ACTION_START
        startService(i)
        bindService(i, mConnection, 0)

    }

    override fun onStop() {
        super.onStop()
        if (serviceBound) {
            if (mediaPlayerService?.isTimerRunning!! || mediaPlayerService!!.songStopped || mediaPlayerService!!.songPaused) {
                mediaPlayerService?.foreground()
            } else {
                stopService(Intent(this, MediaPlayerService::class.java))
            }
            unbindService(mConnection)
            serviceBound = false
        }
    }

    // Handler to update the UI every second when the timer is running
    private val updateTimeHandler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(message: Message) {
            if (MSG_UPDATE_TIME == message.what) {
                updateUITimer()
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS)
                mediaPlayerService!!.createNotification()
            }
        }
    }


      //Updates the UI when a run starts
    private fun updateUIStartRun() {
        updateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME)
    }

    /**
     * Updates the UI when a run stops
     */
    private fun updateUIStopRun() {
        updateTimeHandler.removeMessages(MSG_UPDATE_TIME)
    }

    /**
     * Updates the timer readout in the UI; the service must be bound
     */
    private fun updateUITimer() {
        //updare songName and time
        if (serviceBound) {
            if(mediaPlayerService!!.resetProgressBar){
                processingScope.launch { doProcessing() }
            }
            binding.durationInformation.text = mediaPlayerService!!.helper(1)
            binding.trackTitle.text = mediaPlayerService?.songName
        }

    }



    internal class UIUpdateHandler(activity: MainActivity) : Handler() {
        private val activity: WeakReference<MainActivity> = WeakReference(activity)
        override fun handleMessage(message: Message) {
            if (MSG_UPDATE_TIME == message.what) {
                activity.get()!!.updateUITimer()
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS.toLong())
            }
        }

        companion object {
            private const val UPDATE_RATE_MS = 1000
        }

    }


    //disables pause and stop buttons
    fun disableStopAndPause() {
        binding.pauseButton.isClickable = false;
        binding.stopButton.isClickable = false;
        binding.pauseButton.text = null;
        binding.stopButton.text = null;
    }


    //enables pause and stop buttons
    private fun enableStopAndPause() {
        binding.pauseButton.isClickable = true;
        binding.stopButton.isClickable = true;
        binding.pauseButton.text = getString(R.string.pause)
        binding.stopButton.text = getString(R.string.stop)
    }


    //coroutine for progressbar
    private suspend fun doProcessing() {

        withContext(Dispatchers.Main) {
            binding.progressBar.max = mediaPlayerService?.songLength!!
            if(mediaPlayerService!!.songStopped) binding.progressBar.progress = 0
            else if(mediaPlayerService!!.songPaused){
                binding.progressBar.progress = mediaPlayerService?.previousTime?.toInt()!!
                binding.durationInformation.text = mediaPlayerService!!.helper(0)
            }
        }

        while(mediaPlayerService?.player?.isPlaying!!){

            withContext(Dispatchers.Main) {
                binding.progressBar.progress = mediaPlayerService?.elapsedTime()?.toInt()!!
                if(mediaPlayerService!!.songStopped) binding.progressBar.progress = 0
                else if(mediaPlayerService!!.songPaused) binding.progressBar.progress = mediaPlayerService?.previousTime?.toInt()!!
            }
        }
    }


    //play button
    fun play(v: View) {
        if (serviceBound) {
            if (!mediaPlayerService!!.player.isPlaying) {
                //corutine for progressbar
                processingScope.launch { doProcessing() }

                enableStopAndPause()
                mediaPlayerService?.playSong();

                updateUIStartRun()
            }

        }
    }


    //pause button
    fun pause(v: View) {
        if (serviceBound) {
            if (mediaPlayerService!!.player.isPlaying) {
                mediaPlayerService!!.pauseSong();
                disableStopAndPause()
                updateUIStopRun()
            }

        }
    }


    //stop button
    fun stop(v: View) {
        if (serviceBound) {

            if (mediaPlayerService!!.player.isPlaying) {
                disableStopAndPause()
                mediaPlayerService!!.stopSong();

                //reset text to original text and song title
                binding.durationInformation.text = mediaPlayerService!!.helper(-1)
                binding.trackTitle.text = mediaPlayerService!!.songName

                updateUIStopRun()
            }

        }
    }

    //exitbutton
    fun exit(v: View) {
        if (serviceBound) {
            mediaPlayerService?.exitApplication()
            exitProcess(1)
            finishActivity(1)
        }
    }



    //gestures on button
    fun gon(v: View) {
        if (serviceBound) {
            mediaPlayerService?.gesturesOn()

           //enable goff button
            binding.gofButton.isClickable=true
            binding.gofButton.text= getString(R.string.g_off)

            //disable gon button
            binding.gonButton.isClickable=false
            binding.gonButton.text= null


        }
    }

    //gestures off button
    fun goff(v: View) {
        if (serviceBound) {
            mediaPlayerService?.gesturesOff()

            //enable gon button
            binding.gonButton.isClickable=true
            binding.gonButton.text= getString(R.string.g_on)

            //disable goff button
            binding.gofButton.isClickable=false
            binding.gofButton.text= null


        }
    }


    //update ui based on gestures
    private val messageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Extract data included in the Intent
            val command = intent.getStringExtra("command") //
            if(command == getString(R.string.vertical)) {
                if (serviceBound) {
                    9
                        //corutine for progressbar
                        processingScope.launch { doProcessing() }
                        enableStopAndPause()
                        updateUIStartRun()


                }
            }
            if(command == getString(R.string.horizontal)) {
                if (serviceBound) {
                        disableStopAndPause()
                        updateUIStopRun()


                }
            }
        }
    }


}