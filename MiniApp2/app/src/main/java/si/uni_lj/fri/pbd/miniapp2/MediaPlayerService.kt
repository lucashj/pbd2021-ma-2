package si.uni_lj.fri.pbd.miniapp2

import android.app.*
import android.content.*
import android.graphics.Color
import android.media.MediaPlayer
import android.os.*
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import kotlin.system.exitProcess


class MediaPlayerService : Service() {
    companion object {
        private val TAG: String? = MediaPlayerService::class.simpleName

        const val ACTION_STOP_SONG = "stop_song"
        const val ACTION_PLAY_SONG = "play_song"
        const val ACTION_PAUSE_SONG = "pause_song"
        const val ACTION_EXIT = "exit_application"
        const val ACTION_START = "start_service"

        private const val MSG_UPDATE_TIME = 1
        private const val UPDATE_RATE_MS = 1000L

        private const val channelID = "background_timer"
    }


    var accelerationService: AccelerationService? = AccelerationService()
    var serviceBound: Boolean = false
    var inforeground : Boolean = false

    //builder for notifications
    val builder = NotificationCompat.Builder(this, channelID)
    // Start and end times in milliseconds
    private var startTime: Long = 0
    private var endTime: Long = 0

    var isPlayed : Boolean = false


    //for saving song
    var trenutni : String? = null

    //for updating ui
    var songPaused: Boolean = false;
    var songStopped: Boolean = false;
    var gesturesOn: Boolean = false;
    var resetProgressBar: Boolean = false;


    var songName = ""
    var songLength = 0;
    var previousTime: Long = 0


    var player: MediaPlayer = MediaPlayer();
    var showTimer: Boolean = false;


    private var serviceBinder: RunServiceBinder = RunServiceBinder();


    //handler to call update notification   
    private val updateTimeHandler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(message: Message) {
            if (MediaPlayerService.MSG_UPDATE_TIME == message.what) {
                updateNotification()
                sendEmptyMessageDelayed(MediaPlayerService.MSG_UPDATE_TIME, MediaPlayerService.UPDATE_RATE_MS)

            }
        }
    }





    //for binding to main
    inner class RunServiceBinder : Binder() {
        val service: MediaPlayerService
            get() = this@MediaPlayerService
    }

    override fun onBind(intent: Intent): IBinder {
        return serviceBinder
    }

    override fun onCreate() {
        super.onCreate()
        startTime = 0
        endTime = 0
        isTimerRunning = false
        createNotificationChannel()
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,  IntentFilter("command"));
    }


//recieves message from acceleration service
    private val messageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Extract data included in the Intent
            val command = intent.getStringExtra("command") //
            if(command == getString(R.string.horizontal)){
                pauseSong()
                if(inforeground){
                    showTimer= false
                    stopForeground(true)
                    startForeground(1, createNotification())
                }

            }
            else if(command == getString(R.string.vertical)) {
                playSong()
                isPlayed = true;
                if(inforeground) {
                    showTimer= true
                    stopForeground(true)
                    startForeground(1, createNotification1())
                }
            }

        }
    }


    /**
     * @return whether the timer is running
     */
    // Is the service tracking time?
    var isTimerRunning = false
        private set

    fun startTimer() {
        if (!isTimerRunning) {
            startTime = System.currentTimeMillis()
            isTimerRunning = true
        } else {
            Log.e(MainActivity.TAG, "startTimer request for an already running timer")
        }
    }

    /**
     * Stops the timer
     */
    fun stopTimer() {
        if (isTimerRunning) {
            endTime = System.currentTimeMillis()
            isTimerRunning = false
        } else {
            Log.e(MainActivity.TAG, "stopTimer request for a timer that isn't running")
        }
    }

    /**
     * Returns the  elapsed time
     *
     * @return the elapsed time in seconds
     */
    fun elapsedTime(): Long {
        // If the timer is running, the end time will be zero
        return if (endTime > startTime) ((endTime - startTime) / 1000 + previousTime) else ((System.currentTimeMillis() - startTime) / 1000 + previousTime)
    }


    //calculates minutes and seconds and knows when to restart song
    fun helper(int: Int): String {

        var time = elapsedTime()
        //for updating ui after returning to it from pausing music from notification
        if (int == 0) {
            time = previousTime;
        }
        //for updating ui after returning to it from stopping music from notification
        if (int == -1) {
            time =0;
        }
        var str = ""
        //calculate minutes and seconds for song
        val songLengthMin = songLength?.div(60);
        val songLengthSec = songLengthMin?.times(60).let { songLength.minus(it) }
        //calculate minutes and second for current time
        val timerMin = time.div(60)
        val timerSec = timerMin.times(60).let { time.minus(it) }

        if (elapsedTime() >= songLength) {
            stopTimer()
            resetProgressBar = true;
            previousTime = 0;
            playSong()
        }
        str = java.lang.String.format("%02d:%02d", timerMin, timerSec) + " / " + java.lang.String.format("%02d:%02d", songLengthMin, songLengthSec)
        return str

    }


    //for updating notification called every second from activity main
    fun updateNotification(){
        val managerCompat = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if(showTimer){
            builder.setContentTitle(songName)
            builder.setContentText(helper(1))
            managerCompat.notify(1,builder.build())
        }

    }

    fun changeNotificationButtons(){
        val managerCompat = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if(showTimer){
            builder.setContentTitle(songName)
            builder.setContentText(helper(1))
            managerCompat.notify(1,builder.build())
        }

    }


    //what happens after u press the button in the notification
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(TAG, "Starting service")

        if (intent.action == ACTION_STOP_SONG) {
            stopSong()
            showTimer = false
            stopForeground(true)
            startForeground(1, createNotification())
        } else if (intent.action == ACTION_PAUSE_SONG) {
            pauseSong()
            stopForeground(true)
            showTimer = false

            startForeground(1, createNotification())
        } else if (intent.action == ACTION_PLAY_SONG) {
            playSong()
            showTimer= true

            startForeground(1, createNotification1())
        } else if (intent.action == ACTION_EXIT) {
            exitApplication()
        }
        return START_STICKY

    }

    override fun onDestroy() {
        super.onDestroy()

        player.release()

        Log.d(TAG, "Destroying service")

        //disconnect from acceleration service
        unbindService(sConnection)
        serviceBound = false
    }


    //what happens when u leave the app
    fun foreground() {

        //is playing
        if(player.isPlaying){
            showTimer= true;
            startForeground(1, createNotification1())
        }
        //isnt playing
        else {
            showTimer = false;
            startForeground(1, createNotification())
        }
        inforeground = true;
    }


    //background porcessing
    fun background() {
        inforeground = false;
        showTimer= false;
        stopForeground(true)
    }


    //notification for when music isnt playing
    fun createNotification(): Notification {

        //for playing song
        val actionIntentPlaySong = Intent(this, MediaPlayerService::class.java)
        actionIntentPlaySong.action = ACTION_PLAY_SONG
        val actionPendingIntentPlaySong = PendingIntent.getService(this, 0, actionIntentPlaySong, PendingIntent.FLAG_UPDATE_CURRENT)

        //for exit song
        val actionIntentExitApp = Intent(this, MediaPlayerService::class.java)
        actionIntentExitApp.action = ACTION_EXIT
        val actionPendingIntentExitApp = PendingIntent.getService(this, 0, actionIntentExitApp, PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(this, channelID)
                .setContentTitle(songName)
                .setContentText(helper(0))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setChannelId(channelID)
                .addAction(android.R.drawable.ic_media_pause, "Play", actionPendingIntentPlaySong)
                .addAction(android.R.drawable.ic_media_pause, "Exit", actionPendingIntentExitApp)


        val resultIntent = Intent(this, MainActivity::class.java)
        val resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(resultPendingIntent)
        return builder.build()
    }
    //notification for when music is playing
    fun createNotification1(): Notification {

        //for stopping song
        val actionIntentStopSong = Intent(this, MediaPlayerService::class.java)
        actionIntentStopSong.action = ACTION_STOP_SONG
        val actionPendingIntentStopSong = PendingIntent.getService(this, 0, actionIntentStopSong, PendingIntent.FLAG_UPDATE_CURRENT)

        //for pausing song
        val actionIntentPauseSong = Intent(this, MediaPlayerService::class.java)
        actionIntentPauseSong.action = ACTION_PAUSE_SONG
        val actionPendingIntentPauseSong = PendingIntent.getService(this, 0, actionIntentPauseSong, PendingIntent.FLAG_UPDATE_CURRENT)

        //for exit song
        val actionIntentExitApp = Intent(this, MediaPlayerService::class.java)
        actionIntentExitApp.action = ACTION_EXIT
        val actionPendingIntentExitApp = PendingIntent.getService(this, 0, actionIntentExitApp, PendingIntent.FLAG_UPDATE_CURRENT)

        builder
                .setContentTitle(songName)
                .setContentText(helper(0))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setChannelId(channelID)
                .addAction(android.R.drawable.ic_media_pause, "Pause", actionPendingIntentPauseSong)
                .addAction(android.R.drawable.ic_media_pause, "Stop", actionPendingIntentStopSong)
                .addAction(android.R.drawable.ic_media_pause, "Exit", actionPendingIntentExitApp)


        val resultIntent = Intent(this, MainActivity::class.java)
        val resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(resultPendingIntent)
        return builder.build()
    }




    //create notification channel
    fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT < 26) {
            return
        } else {
            val channel = NotificationChannel(channelID, getString(R.string.channel_name), NotificationManager.IMPORTANCE_LOW)
            channel.description = getString(R.string.channel_desc)
            channel.enableLights(true)
            channel.lightColor = Color.RED
            channel.enableVibration(true)
            val managerCompat = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            managerCompat.createNotificationChannel(channel)
        }
    }

    //choses a random song from assets directory and begins playing it if app just started or if song is over, otherwise continues playing from being stopped or paused
    fun playSong() {
        if(!isTimerRunning){
            //start handler for calling update notification
            updateTimeHandler.sendEmptyMessage(MediaPlayerService.MSG_UPDATE_TIME)
            //if the song was paused -> continue playing
            if (songPaused) {
                startTimer()
                songPaused = false
                player.start()
            }

            //play a random song
            else {
                player.reset()
                //play when not pause or stop were press previously -> find and play a random song
                if(!songStopped){
                    var isRight: Boolean = false;
                    val help: Boolean = true;
                    val listOfMp3 = assets.list("")?.toList<String>();
                    trenutni = listOfMp3?.random()


                    //get a file ending with mp3
                    while (!isRight) {
                        trenutni = listOfMp3?.random()
                        isRight = trenutni?.endsWith("mp3", help)!!
                    }
                }
                //code from here is is song was stopped ->play it again
                songStopped = false
                //confgure and start media player
                val afd = trenutni?.let { assets.openFd(it) }
                if (afd != null) {
                    player.setDataSource(afd?.fileDescriptor, afd.startOffset, afd.length)
                }

                //songName
                songName = trenutni.toString()
                player.prepare()
                //songLength in seconds
                songLength = player.duration / 1000
                startTimer()
                player.start()
            }
        }


    }
//called when pressing pause button
    fun pauseSong() {
        if(!songPaused && isTimerRunning){
            songPaused = true
            stopTimer()

            previousTime = elapsedTime();
            player.pause()
        }
    }
    //called when pressing stop button
    fun stopSong() {
        if(isTimerRunning){
            songStopped = true
            player.stop()
            stopTimer()
            previousTime = 0;
        }

    }

    //called when pressing exit button
    fun exitApplication() {
        //stop itself
        //unbind from acceleration service
        //unbindService(sConnection)
        stopSelf()
        exitProcess(1)

    }


    //conecting to acceleration service
    fun gesturesOn() {
        Toast.makeText(applicationContext,"GESTURES ACTIVATED", Toast.LENGTH_SHORT).show()
        gesturesOn = true;
        val i = Intent(this,AccelerationService::class.java)
        i.action = ACTION_START
        startService(i)
        Log.d(TAG, "Starting and binding service");
        bindService(i, sConnection, 0);
    }

    //disconnect and unbind from acceleraion service
    fun gesturesOff() {
        Toast.makeText(applicationContext,"GESTURES DEACTIVATED", Toast.LENGTH_SHORT).show()
        gesturesOn=false
        stopService(Intent(this, AccelerationService::class.java))
        unbindService(sConnection)

        serviceBound = false
    }


//connection to acceleration service

    private val sConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            Log.d(TAG, "Service bound")
            val binder = iBinder as AccelerationService.RunServiceBinder
            accelerationService = binder.service
            accelerationService!!.background()
            serviceBound = true
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            Log.d(TAG, "Service disconnect")
            serviceBound = false
        }
    }





}